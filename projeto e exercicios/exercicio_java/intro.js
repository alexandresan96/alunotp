console.log("Hello World!");
let variavel = -5;
console.log("variÃ¡vel " + variavel + " Ã© do tipo: " + typeof(variavel));

//operadores matemÃ¡ticos
let numero1 = 5;
let numero2 = 10;

console.log(""+numero1+""+""+numero2);

console.log(5*2);

try{
    let idade = 40;

    if(idade === undefined){
        console.log("Idade nÃ£o foi criada!");
    }else{
        console.log("Sua idade Ã©: " + idade);
    }
}catch(error){
    console.log("Erro na variÃ¡vel idade!"); 
}

function mostrarIdade(){
    let idade = 42;
    if(idade < 42){
        console.log("Jovem");
    }else{
        console.log("NÃ£o muito Jovem");
    }
}
mostrarIdade();

function mostrarIdade2(vlIdade){
    if(vlIdade < 42){
        console.log(vlIdade + " - Jovem");
    }else{
        console.log(vlIdade + " - NÃ£o muito Jovem");
    }
}
mostrarIdade2(42);

const mostraIdade3 = (vlIdade) => {
    if(vlIdade < 42){
        console.log(vlIdade + " - Jovem - versÃ£o 3");
    }else{
        console.log(vlIdade + " - NÃ£o muito Jovem - versÃ£o 3");
    } 
}
mostraIdade3(42);